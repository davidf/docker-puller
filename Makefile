.PHONY: all

DIR := `pwd`

all:
	mkdir -p build
	sudo docker run --rm -v $(DIR):/go/src/docker-puller \
		-v $(DIR)/build:/go/bin golang \
		go install docker-puller

docker: all
	sudo docker build -t pier13/docker-puller .

docker-push: docker
	sudo docker push pier13/docker-puller
