FROM debian:jessie-slim

# Ensure root CAs are installed
RUN apt-get update -y && apt-get install -y --no-install-recommends \
    ca-certificates \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /opt/docker-puller

COPY build/docker-puller /opt/docker-puller/

CMD [ "/opt/docker-puller/docker-puller", \
    "-addr", "0.0.0.0:8080" \
]

EXPOSE 8080
