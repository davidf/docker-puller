Docker Puller
=============

# Overview

A simple utility to update Docker containers to new image versions on command written in Go. The image specified is pulled, and then any containers running that image are stopped, removed, re-created, and started with the new image.  Environment variables, port mappings, volumes, and container name are preserved.

This utility is similar to [Docker Image Puller](https://github.com/tuxity/docker-image-puller). This version is written in Go for single-binary deployment and also as an exercise to learn a bit about the Docker API.  

[Watchtower](https://github.com/v2tec/watchtower) is a more full-featured utility with similar functionality. However, Watchtower works by repeatedly pulling the same image over and over which was not ideal for our use case.  

# Web server

By default the utility starts a web server which can be used to trigger image updates via webhook or similar. Make a `POST` to the `/update` endpoint with a query parameter `key` set to the API key specified on the command line and `image` set to the image name to be pulled.

## Authentication token

Specify an API key on the command line with `-apikey` in order to provide a unique token that will be passed with the HTTP request to authenticate the user. The token is unencrypted so usage of HTTPS is strongly recommended to keep the API key secure. Also, be sure to use a lengthy string of random characters for the API key.

# Command line usage

Starting the program with `-image <image name>` will perform an update of containers running the specified image directly without starting the web server.

# Registry authentication

Command line arguments for specifying a registry user and password can be provided with `-user` and `-pass`. At this time only one set of credentials are supported.

Registry credentials are only used if requested by the registry, so it is possible to use these arguments in order to authenticate to a private registry while also allowing pulling of public images from e.g. Docker Hub.

# Installation

## As a Docker container

```
sudo docker run -d \
    --name docker-puller \
    -e PULLER_APIKEY=XBockFfigDu14cvg7UEyNM \
    -e PULLER_USER=johndoe \
    -e PULLER_PASS=S3cretp4ssw0rd \
    -p 8080:8080 \
    -v /var/run/docker.sock:/var/run/docker.sock \
    --restart="unless-stopped" \
    pier13/docker-puller
```

## Stand-alone binary

```
Usage of docker-puller:
  -addr string
        Web handler listen address (default ":8080")
  -apikey string
        API key for authenticating web requests
  -image string
        Update specified image without web server
  -pass string
        Docker registry password
  -user string
        Docker registry user
```

### Using built-in web server

Start Docker Puller. Assume this is running behind Nginx or similar with HTTPS termination:

```
docker-puller -apikey XBockFfigDu14cvg7UEyNM -user johndoe -pass S3cretp4ssw0rd
```

Then, trigger an update using Curl:

```
curl -X POST "https://docker.example.com/update?key=XBockFfigDu14cvg7UEyNM&image=portainer/portainer"
```

### Using command line

```
docker-puller -image portainer/portainer
```
