package main

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"flag"
	"io"
	"log"
	"net/http"
	"os"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/client"
)

var config struct {
	addr   string
	apikey string
	user   string
	pass   string
	image  string
}

func init() {
	flag.StringVar(&config.addr, "addr", ":8080", "Web handler listen address")
	flag.StringVar(&config.apikey, "apikey", "", "API key for authenticating web requests")
	flag.StringVar(&config.user, "user", "", "Docker registry user")
	flag.StringVar(&config.pass, "pass", "", "Docker registry password")
	flag.StringVar(&config.image, "image", "", "Update specified image without web server")
	flag.Parse()

	// Use environment variables for any items not found on command line
	if config.apikey == "" {
		config.apikey = os.Getenv("PULLER_APIKEY")
	}
	if config.user == "" {
		config.user = os.Getenv("PULLER_USER")
	}
	if config.pass == "" {
		config.pass = os.Getenv("PULLER_PASS")
	}

	// Ensure either command-line image name was specified or else an API key
	// was specified to run in web mode
	if config.image == "" && config.apikey == "" {
		log.Fatalf("You must specify an image name or an API key, see -help")
	}
}

func main() {
	log.Printf("docker-puller started")

	// If an image was specified on the command line, update it directly,
	// otherwise start a web server to wait for requests to update images
	if config.image != "" {
		updateContainers(os.Stdout, config.image)
	} else {
		log.Printf("Listening on %s", config.addr)
		http.HandleFunc("/update", updateHandler)
		log.Fatal(http.ListenAndServe(config.addr, nil))
	}
}

// updateHandler receives requests via HTTP and if valid will call
// updateContainers to pull the new container image and recreate the
// containers which match the image passed in via a query parameter.
func updateHandler(rw http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(rw, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	key := r.URL.Query().Get("key")
	if key != config.apikey {
		http.Error(rw, "Unauthorized", http.StatusUnauthorized)
		return
	}

	imageName := r.URL.Query().Get("image")
	if imageName == "" {
		http.Error(rw, "Invalid image name", http.StatusBadRequest)
		return
	}

	updateContainers(rw, imageName)
}

// updateContainers will pull a new version of the specified image and then
// stop, remove, and start any containers using that image. Environment,
// network, and volume settings from the existing running container are
// reused, as is the container name.
func updateContainers(out io.Writer, imageName string) error {
	logger := log.New(out, "", log.LstdFlags)

	ctx := context.Background()
	cli, err := client.NewEnvClient()
	if err != nil {
		log.Println("Error creating Docker client:", err)
		return err
	}

	containers, err := cli.ContainerList(ctx, types.ContainerListOptions{})
	if err != nil {
		log.Println("Error listing containers:", err)
		return err
	}

	// Pull the new image
	privFunc := func() (string, error) {
		authConfig := types.AuthConfig{
			Username: config.user,
			Password: config.pass,
		}
		encodedJSON, err := json.Marshal(authConfig)
		if err != nil {
			log.Println("Error encoding JSON auth string:", err)
			return "", err
		}
		return base64.URLEncoding.EncodeToString(encodedJSON), nil
	}
	pullOpts := types.ImagePullOptions{
		PrivilegeFunc: privFunc,
	}

	pull, err := cli.ImagePull(ctx, imageName, pullOpts)
	if err != nil {
		// Retry again with auth information provided directly to work around
		// Gitlab issue. Without authorization information provided, Gitlab
		// returns a 403 Access Forbidden error, yet the ImagePull code in the
		// Docker API only retries automatically for 401 Unauthorized.
		pullOpts.RegistryAuth, _ = privFunc()
		pull, err = cli.ImagePull(ctx, imageName, pullOpts)
		if err != nil {
			log.Println("Error pulling new image:", err)
			return err
		}
	}
	defer pull.Close()
	io.Copy(out, pull)
	logger.Printf("Pulled: %s", imageName)

	// Find any container instances using the specified image
	logger.Printf("Finding containers for: %s", imageName)
	for _, cnt := range containers {
		info, _ := cli.ContainerInspect(ctx, cnt.ID)

		if info.Config.Image == imageName {
			// Found a matching image
			logger.Printf("Found: %s (%s)", cnt.ID, info.Name)

			// Stop the existing container
			err = cli.ContainerStop(ctx, cnt.ID, nil)
			if err != nil {
				log.Println("Error stopping existing container:", err)
				return err
			} else {
				logger.Printf("Stopped: %s", cnt.ID)
			}

			// Remove the existing container
			err = cli.ContainerRemove(ctx, cnt.ID, types.ContainerRemoveOptions{})
			if err != nil {
				log.Println("Error removing existing container:", err)
				return err
			} else {
				logger.Printf("Removed: %s", cnt.ID)
			}

			// Create a new container using the same config settings that the
			// stopped container used
			rsp, err := cli.ContainerCreate(ctx,
				&container.Config{
					Image: imageName,
					Env:   info.Config.Env,
				},
				info.HostConfig, nil, info.Name)
			if err != nil {
				log.Println("Error creating new container:", err)
				return err
			} else {
				logger.Printf("Created: %s", rsp.ID)
			}

			// Start the new container that was just created
			err = cli.ContainerStart(ctx, rsp.ID, types.ContainerStartOptions{})
			if err != nil {
				log.Println("Error starting new container:", err)
				return err
			} else {
				logger.Printf("Started: %s", rsp.ID)
			}
		}
	}

	return err
}
